package com.juwitatheomas_10191043.presistence;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.prefs.Preferences;

public class MainActivity extends AppCompatActivity {

    private TextView Hasil;
    private EditText Masukan;
    private Button Eksekusi;

    private SharedPreferences preferences;

    private SharedPreferences.Editor editor;
    private Object Preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Masukan = findViewById(R.id.input);
        Hasil = findViewById(R.id.output);
        Eksekusi = findViewById(R.id.save);

        Preferences = getSharedPreferences("Belajar_SharedPreferences",Context.MODE_PRIVATE);

        Eksekusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View view){
                getData();
                Toast.makeText(getApplicationContext(), "Data Tersimpan", Toast.LENGTH_SHORT).show();
            }

            private void getData() {

                String getKonten = Masukan.getText().toString();
                editor = preferences.edit();
                editor.putString("data_Saya", getKonten);

                editor.apply();
                Hasil.setText("Output Data :"+preferences.getString("data_saya",null));
            }
        });

    }

}